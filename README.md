# Galaxy Installation scripts
This script dedicated to establish a clean and orderly working *Galaxy* environment in a reproducible manner.
## Script Information

### Basic info
* Language
 - Ansible 1.9 (YML-formatted playbooks) 
* Tested work environment
 - Ubuntu 14.04
 - SLES 11 SP3 (Suse Linux Enterprise Server)
* Execution time
 - ~15 minutes (incl. all compilations)

### Prerequisites/Assumptions
1. All hosts must be accessable via *SSH*
2. All hosts must have identical root password.
3. Internet connection for downloads
4. Script deployment machine must have *Ansible*, *ansible-modules-core* and *ansible-modules-extras* integrated.
 - run ./ansible_installation.sh in order to save some time.
 
### Other info
Our scripts can be deployed on a blank SLES/Ubuntu machines.
 * For SLES, core and devel zypper repositories must be integrated in blank machine before script execution.

## Installation
The following files have to be configured before deploying the scripts:
1. Download our scripts
```sh
$ git clone https://anazeer@bitbucket.org/ibe/galaxy-setup-scripts.git
```
2. Configure "hosts.sample" and save it as "hosts". Instructions are in "hosts.sample" file.
3. Configure "inifile.yml.sample" and save it as "inifile.yml". Instructions are in "inifile.yml.sample" file.
```sh
$ cd galaxy-setup-scripts
$ chmod +x install.sh
$ ./install.sh
```
4. Select your choice of installation and enter the root password of the hosts(identical root password)

## For minimum configuration
1. Add your host name to [galaxyhost] group in "hosts.sample" file and save it as "hosts"
2. Comment everything else in "inifile.yml.sample" file and save it as "inifile.yml", except the following variables,
  - GALAXY_USER
  - GALAXY_USER_ID
  - NGS_GROUP
  - NGS_GROUP_ID
3. run "./ansible_installation.sh" (if ansible is not installed)
4. run "./install.sh" and select "galaxy"(number 5) for installing galaxy alone.

## Variables info

* All end user configurable variables are in inifile.yml
* Those variables not in inifile.yml can be found in galaxy-setup-scripts/roles/common/vars/main.yml

## After galaxy installation
the uppercase characters are the variables you set in inifile.yml, if you have edit only the inifile.yml.

### Run galaxy
* In command line
```sh
$ cd /home/GALAXY_USER/galaxy
$ sh run.sh
```
* In web browser
https://localhost:PROXY_PORT

*Note:
 - Please replace GALAXY_USER and PROXY_PORT variables from inifile.yml before running it.
 - Please replace "localhost" with the appropriate "ip" address or your "full hostname with domain address"

Email us if you need any questions... see "contributors.txt"
Please mention "URTOOL" as the first word in the subject of your email.
Thank you
