#!/usr/bin/python

import os
import subprocess

class py_home(object):
    def __init__(self,src,dest,module):
        self.src = src
        self.dest = dest
        self.module = module
    def py_homescheme(self):
        proc = subprocess.Popen('source ~/.bashrc; python "%s" install --home="%s" --prefix=""' % (self.src, self.dest), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell=True)
        self.out = proc.communicate()

def main():
    module =  AnsibleModule(
        argument_spec = dict(
            src               = dict(required=True),
            original_basename = dict(required=False), # used to handle 'dest is a directory' via template, a slight hack
            dest              = dict(required=True),
        ),
    add_file_common_args=True,
    )
    src = os.path.expanduser(module.params['src'])
    src_dir = src
    src = src + '/setup.py'
    dest = os.path.expanduser(module.params['dest'])
    # if file present..
    if not os.path.exists(src):
        module.fail_json(msg="Source '%s' does not exist" % src)
    if not os.access(src, os.R_OK):
        module.fail_json(msg="Source '%s' not readable" % src)
    if not os.path.isdir(dest):
        module.fail_json(msg="Destination '%s' is not a directory" % dest)
    if not os.access(dest, os.W_OK):
        module.fail_json(msg="Destination '%s' not writable" % dest)
    homescheme = py_home(src,dest,module)
    try:
        os.chdir(src_dir)
        homescheme.py_homescheme()
        module.exit_json(changed=True)
    except IOError:
        module.fail_json(msg="failed to install %s as home scheme" % src)

from ansible.module_utils.basic import *
main()
