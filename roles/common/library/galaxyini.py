#!/usr/bin/python

import os
import re

class Galaxyini(object):
    def __init__(self, src, line, group, commentonly, force, module):
        self.src = src
        self.line = line
        self.group = group
        self.commentonly = commentonly
        self.force = force
        self.module = module

    def get_tags(self):
        tmp_line = self.line
        if '=' in tmp_line:
            tmp_line = tmp_line.replace(' ', '')
            self.key = tmp_line.split('=')[0]
            self.value = tmp_line.split('=')[1]
        else:
            self.key = tmp_line
            self.value = None

    def get_lines(self):
        self.get_tags()
        self.data = open(self.src, 'r').read().splitlines()
        self.dest1 = []
        self.dest3 = []
        enter_var = 0
        lin_num = 0
        self.stat = None
        for li in self.data:
            lin_num += 1
            if not re.search('^\[' + self.group + '\]', li) and enter_var == 0:
                self.dest1.append(li)
            elif re.search('^\[' + self.group + '\]', li) and enter_var == 0:
                self.lin_start = lin_num
                enter_var = 1
            elif re.search('^\[', li) and enter_var == 1:
                self.lin_end = lin_num
                enter_var = 2
                self.dest3.append(li)
            elif enter_var == 2:
                self.dest3.append(li)
        if enter_var == 0:
            self.stat = 'ERROR1'
        elif enter_var == 1:
            self.lin_end = lin_num + 1
        if self.commentonly == True and self.force == True:
            self.stat = 'ERROR2'

    def comment_lines(self):
        self.dest2_tmp = []
        lin_num = 0
        self.run_code = None
        self.stat = None
        for li in self.data[(self.lin_start-1):(self.lin_end-1)]:
            if self.value and re.search('^' + self.key + '=' + self.value, li.replace(' ','')) and self.commentonly == True:
                self.dest2_tmp.append('# ' + li)
                self.run_code = 1
                self.stat = 'CHANGED'
            elif self.value and re.search('^' + self.key + '=' + self.value, li.replace(' ','')) and self.commentonly == False:
                self.run_code = 2
                self.stat = 'OK'
                break
            elif self.value and re.search('^#' + self.key + '=' + self.value, li.replace(' ','')) and self.commentonly == True:
                self.run_code = 2
                self.dest2_tmp.append(li)
                if self.stat != 'CHANGED':
                    self.stat = 'OK'
            elif self.value and re.search('^#' + self.key + '=' + self.value, li.replace(' ','')) and self.commentonly == False:
                self.dest2_tmp.append(li)
                self.run_code = 1
                self.stat = 'CHANGED'
            elif re.search('^' + self.key + '=', li.replace(' ','')):
                self.dest2_tmp.append('# ' + li)
                self.run_code = 1
                self.stat = 'CHANGED'
            elif re.search('^#' + self.key + '=', li.replace(' ','')):
                self.dest2_tmp.append(li)
                self.run_code = 1
                self.stat = 'CHANGED'
            else:
                self.dest2_tmp.append(li)
        if not self.run_code:
            self.run_code = 3
            self.stat = None

    def append_line(self):
        self.dest2 = []
        append_var = 0
        if self.commentonly == False:
            for li in self.dest2_tmp:
                self.dest2.append(li)
                if re.search('^#' + self.key + '=', li.replace(' ','')) and append_var == 0:
                    self.dest2.append(self.key + ' = ' + self.value)
                    self.stat = 'CHANGED'
                    append_var = 1
                elif re.search('^#' + self.key + '=', li.replace(' ','')) and append_var == 1:
                    continue
        else:
            self.dest2 = self.dest2_tmp
        if self.run_code == 3 and self.force == True:
            #self.dest2.append(self.key + ' = ' + self.value)
            tmp_dest2_val = []
            for val in self.dest2:
                tmp_dest2_val.append(val)
                if self.dest2[0] == val:
                    tmp_dest2_val.append('\n' + self.key + ' = ' + self.value)
            self.dest2 = tmp_dest2_val
            self.stat = 'CHANGED'
        elif self.run_code == 3 and self.force == False:
            self.stat = 'ERROR'
        if self.stat == 'CHANGED':
            editfile = open(self.src + '.tmp', 'w')
            editfile.write('\n'.join(self.dest1 + self.dest2 + self.dest3 + ['']))
            editfile.close()
            #if self.src.split('.')[len(self.src.split('.'))-1] == 'sample':
            #    tmp_src = '.'.join(self.src.split('.')[:(len(self.src.split('.'))-1)])
            #else:
            #    tmp_src = self.src
            os.system('cp %s %s' % (self.src, self.src + '.bckp'))
            os.system('mv %s %s' % (self.src + '.tmp', self.src))

def main():
    module = AnsibleModule(
        argument_spec = dict(
            src = dict(required=True),
            line = dict(required=True),
            group = dict(default=True),
            commentonly = dict(default=False, type='bool'),
            force = dict(default=False, type='bool'),
        ),
        add_file_common_args=True,
    )

    src = os.path.expanduser(module.params['src'])
    line = module.params['line']
    group = module.params['group']
    commentonly = module.params['commentonly']
    force = module.params['force']

    if not os.path.exists(src):
        module.fail_json(msg="Source %s does not exist" % src)
    if not os.access(src, os.R_OK):
        module.fail_json(msg="Source %s not readable" % src)

    file_edit = Galaxyini(src, line, group, commentonly, force, module)

    try:
        file_edit.get_tags()
        file_edit.get_lines()
        if file_edit.stat == 'ERROR1':
            module.fail_json(msg="Couldn't find the group '%s' specified in file %s" %(group, src))
        if file_edit.stat == 'ERROR2':
            module.fail_json(msg="Cannot use commentonly=True and force=True togather")
        file_edit.comment_lines()
        if file_edit.stat == 'OK':
            module.exit_json(changed=False)
        file_edit.append_line()
        if file_edit.stat == 'CHANGED':
            module.exit_json(changed=True)
        if file_edit.stat == 'ERROR' and commentonly == False:
            module.fail_json(msg="Couldn't find the key '%s' specified in file %s. Use force=True to force the change." %(file_edit.key, src))
        if file_edit.stat == 'ERROR' and commentonly == True:
            module.exit_json(changed=False)
    except IOError:
        module.fail_json(msg="failed to edit %s" % src)

from ansible.module_utils.basic import *
main()