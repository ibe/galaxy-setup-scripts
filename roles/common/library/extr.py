#!/usr/bin/python

import os
import shutil

def var_tar(self):
    cmd = 'tar -xvf "%s" -C "%s"' % (self.src, self.dest)
    rc, out, err = self.module.run_command(cmd)
    return dict(cmd=cmd, rc=rc, out=out, err=err)

def var_targz(self):
    cmd = 'tar -xzvf "%s" -C "%s"' % (self.src, self.dest)
    rc, out, err = self.module.run_command(cmd)
    return dict(cmd=cmd, rc=rc, out=out, err=err)

def var_tarbz2(self):
    cmd = 'tar -xjvf "%s" -C "%s"' % (self.src, self.dest)
    rc, out, err = self.module.run_command(cmd)
    return dict(cmd=cmd, rc=rc, out=out, err=err)

def var_tgz(self):
    cmd = 'tar xzvf "%s" -C "%s"' % (self.src, self.dest)
    rc, out, err = self.module.run_command(cmd)
    return dict(cmd=cmd, rc=rc, out=out, err=err)

def var_zip(self):
    cmd = 'unzip -n "%s" -d "%s"' % (self.src, self.dest)
    rc, out, err = self.module.run_command(cmd)
    return dict(cmd=cmd, rc=rc, out=out, err=err)

def var_gz(self):
    tmp_src = self.src.split('.')
    tmp_src = '.'.join(tmp_src[:len(tmp_src)-1])
    cmd = 'gunzip "%s"' % (self.src)
    rc, out, err = self.module.run_command(cmd)
    if rc == 0:
        shutil.move(tmp_src, self.dest)
    return dict(cmd=cmd, rc=rc, out=out, err=err)

def unidentified(self):
    return dict(out='unidentifed ARCHIVE extension')

class Archive_Extraction(object):
    def __init__(self, src, dest, module):
        self.src = src
        self.dest = dest
        self.module = module
    def archive_extension(self):
        filename = os.path.basename(self.src)
        ext_types = [ 'tar', 'gz', 'zip', 'bz2', 'tgz', 'jar' ]
        chunks = filename.split(".")
        ext_num = 0
        for i in range(len(chunks)-1,-1,-1):
            if chunks[i] in ext_types:
                ext_num += 1
            else:
                break
        if ext_num == 0:
            self.ext_type = "None"
        else:
            self.ext_type = '.'.join(chunks[len(chunks)-ext_num:])
        return self.ext_type
    def archive_extraction(self):
        self.archive_extension()
        options = { 'tar' : var_tar, 'tar.gz' : var_targz, 'tar.bz2' : var_tarbz2, 'tgz' : var_tgz, 'zip' : var_zip, 'gz' : var_gz , 'None' : unidentified }
        options[self.ext_type](self)

def main():
    module =  AnsibleModule(
        argument_spec = dict(
            src               = dict(required=True),
            original_basename = dict(required=False), # used to handle 'dest is a directory' via template, a slight hack
            dest              = dict(default='None'),
            copy              = dict(default=True, type='bool'),
            creates           = dict(required=False),
        ),
        add_file_common_args=True,
    )
    src = os.path.expanduser(module.params['src'])
    dest = os.path.expanduser(module.params['dest'])
    if dest == 'None':
        dest = os.path.dirname(src)
    copy = module.params['copy']
    # if file present..
    if not os.path.exists(src):
        if copy:
            module.fail_json(msg="Source '%s' failed to transfer" % src)
        else:
            module.fail_json(msg="Source '%s' does not exist" % src)
    if not os.access(src, os.R_OK):
        module.fail_json(msg="Source '%s' not readable" % src)
    # if dest present..
    if not os.path.isdir(dest):
        module.fail_json(msg="Destination '%s' is not a directory" % dest)
    if not os.access(dest, os.W_OK):
        module.fail_json(msg="Destination '%s' not writable" % dest)
    extr = Archive_Extraction(src, dest, module)
    try:
        extr.archive_extraction()
        if extr.ext_type == 'None':
            module.exit_json(changed=False, ext_type=extr.ext_type, filename='')
        else:
            tmp_src = src.split('.')
            ext_type_len = len(extr.ext_type.split('.'))
            tmp_src = '.'.join(tmp_src[:len(tmp_src)-ext_type_len])
            if dest == 'None':
                tmp_filename = tmp_src
            else:
                tmp_filename = dest + '/' + os.path.basename(tmp_src)
            module.exit_json(changed=True, ext_type=extr.ext_type, filename=tmp_filename)
    except IOError:
        module.fail_json(msg="failed to unpack %s to %s" % (src, dest))

from ansible.module_utils.basic import *
main()