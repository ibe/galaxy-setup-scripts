#!/usr/bin/python

import os
import re
import time
import commands
import subprocess

class Galaxy_Run(object):
    def __init__(self,src,state,module):
        self.src = src
        self.state = state
        self.module = module

    def galaxy_start(self):
        runfile = self.src + '/run.sh'
        pidfile = self.src + '/paster.pid'
        logfile = self.src + '/paster.log'
        self.galaxy_run_check()
        if os.path.exists(runfile) and self.running == 'no':
            tmp_run = subprocess.Popen('. ~/.bashrc; sh %s --daemon' % runfile, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            tmp_m = tmp_run.communicate()
            tmp_var1 = 0
            while tmp_var1 <= 9:
                tmp_var1 += 1
                try:
                    pid = open(pidfile, 'r').read()
                except IOError:
                    pid = 'None'
                if pid != 'None':
                    for line in open(logfile, 'r'):
                        if re.search('Starting server in PID ' + pid, line):
                            break
                time.sleep(12)
                if tmp_var1 == 4 and pid == 'None':
                    break

    def galaxy_run_check(self):
        pidfile = self.src + '/paster.pid'
        if os.path.exists(pidfile):
            self.running = 'yes'
            return dict(out='galaxy is running ...')
        else:
            self.running = 'no'
            return dict(out='galaxy is not running ...')

    def galaxy_stop(self):
        runfile = self.src + '/run.sh'
        pidfile = self.src + '/paster.pid'
        self.galaxy_run_check()
        if self.running == 'yes':
            cmd = 'sh %s --stop-daemon' % runfile
            rc, out, err = self.module.run_command(cmd)
            return dict(cmd=cmd, rc=rc, out=out, err=err)
            if os.path.exists(pidfile):
                return dict(err='galaxy is still running ...')

def main():
    module =  AnsibleModule(
        argument_spec = dict(
            src               = dict(required=True),
            original_basename = dict(required=False), # used to handle 'dest is a directory' via template, a slight hack
            state             = dict(required=True),
        ),
        add_file_common_args=True,
    )

    src = os.path.expanduser(module.params['src'])
    state = module.params['state']

    if not os.path.isdir(src):
        module.fail_json(msg="Destination '%s' is not a directory" % src)
    if not os.access(src, os.W_OK):
        module.fail_json(msg="Destination '%s' not writable" % src)

    state_var = [ 'started' , 'stopped' , 'restarted' ]

    rungalaxy = Galaxy_Run(src,state,module)

    try:
        if state == 'restarted':
            rungalaxy.galaxy_stop()
            rungalaxy.galaxy_start()
        elif state == 'started':
            rungalaxy.galaxy_start()
            if rungalaxy.running == 'yes':
                module.exit_json(changed=False, state=state)
        elif state == 'stopped':
            rungalaxy.galaxy_stop()
            if rungalaxy.running == 'no':
                module.exit_json(changed=False, state=state)
        module.exit_json(changed=True, state=state)
    except IOError:
        module.fail_json(msg="state=%s failed" % state)

from ansible.module_utils.basic import *
main()
