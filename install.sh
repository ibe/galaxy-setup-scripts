#!/bin/bash
# Checking root
if [ ! "$(whoami)" == "root" ];
then
    echo -e "\nPlease run as root...\n"; exit 1
fi
 
# Checking the current working directory
if [ ! -f "${PWD}/site.yml" ];
then
    echo -e "\nFile playbooks.yml missing...Please check the working directory\n"
    exit 1
fi

if [ ! -f "${PWD}/inifile.yml" ] || [ ! -f "${PWD}/hosts" ];
then
    echo -e "\nplease create 'inifile.yml' and 'hosts' files from 'inifile.yml.sample' and 'hosts.sample' files and run ./install.sh ...\n"
    exit 1
fi

export ANSIBLE_LIBRARY=${PWD}/ansible-modules-core:${PWD}/ansible-modules-extras

OPTIONS="galaxy-apache2-postgresql-nfs_server_setup-client_connection_setup-galaxy_grid_configuration galaxy-apache2-postgresql-nfs_server_setup-client_connection_setup galaxy-apache2-postgresql-client_connection_setup galaxy-apache2-postgresql galaxy apache2 postgresql nfs_server_setup client_connection_setup galaxy_grid_configuration exit"

select OPT_VAR in ${OPTIONS};
do
    case ${OPT_VAR} in
        'galaxy-apache2-postgresql-nfs_server_setup-client_connection_setup-galaxy_grid_configuration')
            addvars="galaxy=True apache2=True postgresql=True nfs_server_setup=True client_connection_setup=True galaxy_grid_configuration=True"
            break
         ;;
        'galaxy-apache2-postgresql-nfs_server_setup-client_connection_setup')
            addvars="galaxy=True apache2=True postgresql=True nfs_server_setup=True client_connection_setup=True"
            break
         ;;
        'galaxy-apache2-postgresql-client_connection_setup')
            addvars="galaxy=True apache2=True postgresql=True client_connection_setup=True"
            break
         ;;
        'galaxy-apache2-postgresql')
            addvars="galaxy=True apache2=True postgresql=True"
            break
         ;;
        'galaxy_grid_configuration')
            addvars="galaxy_grid_configuration=True"
            break
         ;;
        'galaxy')
            addvars="galaxy=True"
            break
         ;;
        'apache2')
            addvars="apache2=True"
            break
         ;;
        'postgresql')
            addvars="postgresql=True"
            break
         ;;
        "nfs_server_setup")
            addvars="nfs_server_setup=True"
            break
         ;;
        "client_connection_setup")
            addvars="client_connection_setup=True"
            break
         ;;
        'galaxy_grid_configuration')
            addvars="galaxy_grid_configuration=True"
            break
         ;;
        'exit')
            echo "See you later...:-)"
            exit 0;;
        *)
            echo "The selected option is not valid..."
            exit 1;;
    esac
done

if [ $(apt-get | echo $?) == 0 ]; then 
apt-get install -y python-dev python-jinja2 python-pkg-resources python-paramiko python-yaml python-crypto

elif [ $(zypper | echo $?) == 0 ]; then
zypper -n install python-devel python-Jinja2 python-paramiko python-yaml python-crypto

fi
echo "Dont panic... Let it run..."

echo $addvars
export ANSIBLE_LIBRARY=${PWD}/ansible-modules-core:${PWD}/ansible-modules-extras

echo "Please enter the root password..."
ansible-playbook site.yml -i hosts -k -e "LOCAL_DIR=${PWD} ${addvars}"
