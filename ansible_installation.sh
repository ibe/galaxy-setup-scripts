#!/bin/bash

# Checking root
if [ ! "$(whoami)" == "root" ];
then
    echo -e "\nPlease run as root...\n"; exit 1
fi

# Checking the current working directory
if [ ! -f "${PWD}/site.yml" ];
then
    echo -e "\nFile site.yml missing...Please check the working directory\n"
    exit 1
fi

LOCAL_DIR=${PWD}

if [ $(apt-get | echo $?) == 0 ]; then
apt-get update
apt-get install -y git ssh python-setuptools sshpass
elif [ $(zypper | echo $?) == 0 ]; then
zypper -n install git python-setuptools
fi
echo "Let it run..."
# cloning ansible
cd ${LOCAL_DIR}
if [ ! -d "ansible" ];
then
git clone https://github.com/ansible/ansible.git
fi
if [ ! -d "ansible-modules-core" ];
then
git clone https://github.com/ansible/ansible-modules-core.git
fi
if [ ! -d "ansible-modules.extras" ];
then
git clone https://github.com/ansible/ansible-modules-extras.git
fi

cd ${LOCAL_DIR}/ansible
git checkout stable-1.9
python setup.py install

# cloning modules
cd ${LOCAL_DIR}/ansible-modules-core
git checkout stable-1.9
cd ${LOCAL_DIR}/ansible-modules-extras
git checkout stable-1.9

export ANSIBLE_LIBRARY=${LOCAL_DIR}/ansible-modules-core:${LOCAL_DIR}/ansible-modules-extras

cd ${LOCAL_DIR}
chmod +x install.sh

sed -i "s/PermitRootLogin without-password/PermitRootLogin yes/" /etc/ssh/sshd_config

service ssh restart

